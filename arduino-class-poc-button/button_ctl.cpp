 #include "Arduino.h"
 #include "button_ctl.h" 


    Button_ctl::Button_ctl(     enTypeofButton  new_buttonType,
                                enActionButton  new_buttonAction,
                                enStateOfButton new_buttonState,
                                enActionUntilRelease  new_buttonActionPressRelease,
                                int new_holdTime, 
                                int new_buttonPinAttached,
                                int new_loopTime
                                ){
         buttonType = new_buttonType;
         buttonAction= new_buttonAction;
         buttonState= new_buttonState;
         buttonStatus = buttonState;
         holdTime= (new_holdTime - 1) * 1000; //time in seconds 
         holdTime = holdTime / new_loopTime;
         holdCounter= 0; 
         buttonPinAttached= new_buttonPinAttached;
         loopTime= new_loopTime;
         button_ActionUntilRelease = new_buttonActionPressRelease;
         pinMode(buttonPinAttached,INPUT);
    }

   int Button_ctl::get_button_status(void){
        int status_button = (int) buttonStatus;
        buttonStatus = btn_released;
        return status_button;
    }


    void Button_ctl::func_high_low_activation(enTypeofButton  buttonType){
      int input_pin_val = digitalRead(buttonPinAttached);
      if(buttonType == normally_open ){
        (input_pin_val == 1) ? (buttonState = btn_released) : (buttonState = btn_pressed);
      }
      // normally_close
      else{
        (input_pin_val == 0) ? (buttonState = btn_released) : (buttonState = btn_pressed);
      }
    }
    
    void Button_ctl::func_low_high_activation(enTypeofButton buttonType){
      int input_pin_val = digitalRead(buttonPinAttached);
      if(buttonType == normally_open ){
        (input_pin_val == 0) ? (buttonState = btn_released) : (buttonState = btn_pressed);
      }
      // normally_close
      else{
        (input_pin_val == 1) ? (buttonState = btn_released) : (buttonState = btn_pressed);
      }
    }


    void Button_ctl::func_normally_open(void){
      switch ((int) buttonAction){
        case  (int) high_low_activation:
            func_high_low_activation(normally_open);
        break;
        case  (int) low_high_activation:
            func_low_high_activation(normally_open);
        break;
       }
    }

    void Button_ctl::func_normally_close(void){
            switch ((int) buttonAction){
        case  (int) high_low_activation:
            func_high_low_activation(normally_close);
        break;
        case  (int) low_high_activation:
            func_low_high_activation(normally_close);
        break;
       }
    }

    void Button_ctl::loop_check_button(void){
       switch ((int) buttonAction){
        case  (int) normally_open:
            func_normally_open();   
        break;
        case  (int) normally_close:
            func_normally_close();
        break;
       }
      if(button_ActionUntilRelease == enAURelease){ 
          if(buttonState == btn_released){
            if(holdCounter >= holdTime){
               buttonStatus = btn_pressedAndHold;
            }else if(holdCounter > 0){
               buttonStatus = btn_pressedAndReleased;
            }
              holdCounter = 0;
          }
          else if (buttonState == btn_pressed){
             holdCounter++;
          }
      }
       else{
        if(buttonState == btn_released){
           
              holdCounter = 0;
          }
          else if (buttonState == btn_pressed){
               holdCounter++;
              if(holdCounter >= holdTime){
               buttonStatus = btn_pressedAndHold;
              }
              else if(holdCounter > 0){
               buttonStatus = btn_pressedAndReleased;
            }
          }
        }
         
    }

    
