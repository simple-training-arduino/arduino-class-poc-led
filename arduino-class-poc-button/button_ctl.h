#ifndef Button_h
#define Button_h


#include "Arduino.h"

enum enTypeofButton{
  normally_open,
  normally_close
};

enum enActionButton{
  high_low_activation,
  low_high_activation
};

enum enStateOfButton{
  btn_released,
  btn_pressed,
  btn_pressedAndReleased,
  btn_pressedAndHold
};

enum enActionUntilRelease{
  enAURelease,
  enAUPress
};

class Button_ctl {

  private:
    enTypeofButton  buttonType;
    enActionButton  buttonAction;
    enStateOfButton buttonState,buttonStatus;
    enActionUntilRelease button_ActionUntilRelease;
    int holdTime; 
    int holdCounter; 
    int buttonPinAttached;
    int loopTime;
    
    void func_high_low_activation(enTypeofButton  buttonType);
    void func_low_high_activation(enTypeofButton buttonType);
    void func_normally_open(void);
    void func_normally_close(void);   

  public:
    Button_ctl(     enTypeofButton  new_buttonType,
                    enActionButton  new_buttonAction,
                    enStateOfButton new_buttonState,
                    enActionUntilRelease  new_buttonActionPressRelease,
                    int new_holdTime, 
                    int new_buttonPinAttached,
                    int new_loopTime 
                    );
    void loop_check_button(void);
 
    int get_button_status(void);
};

#endif
