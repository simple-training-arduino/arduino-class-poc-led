#include "button_ctl.h" //include the class
#define LOOP_TIME       100
#define HOLD_TIME_SEC   4

 Button_ctl s1_Button (     normally_open,
                            high_low_activation,
                            btn_released,
                            enAUPress,
                            HOLD_TIME_SEC, 
                            A1,
                            LOOP_TIME 
                    );
int button_old_status,button;
void setup() {
  // put your setup code here, to run once:
    Serial.begin(115200);
    button_old_status = 3;
}

void loop() {
  // put your main code here, to run repeatedly:
   button = s1_Button.get_button_status();
   if(button != button_old_status){
        Serial.print("Btn state: ");
        Serial.println(button);
        button_old_status = button;
   }

   s1_Button.loop_check_button();
  delay(LOOP_TIME);
}
