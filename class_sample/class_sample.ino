// C++ code
//

#include "Myled.h"

#define mainLoopTime 100

Leds led_red(LED_BUILTIN,mainLoopTime,led_on);

/*
 *     void set_led_on();
    void set_led_off();
    int get_led_status();
    void set_led_blink_on(int new_set_count, int new_period);
    void set_led_blink_off(int new_set_count, int new_period); 
 */
  
void setup()
{
  Serial.begin(9600);
   delay(1000);
 // led_red.set_led_off(); //outputs 0
 // led_red.set_led_on();  //outputs 1
    led_red.set_led_blink_on(5,1);
  //led_red.set_led_blink_off(6, 1);
  Serial.println(led_red.get_led_pin());
    
}

void loop()
{

  led_red.execute_led_blink();
  delay(mainLoopTime); // Wait for 1000 millisecond(s)
}
