 #include "Arduino.h"
 #include "Myled.h" 
  
  Leds::Leds(int new_led_pin, int new_mainloopTimer, int new_ledstatus ){
      led_pin = new_led_pin;
      pinMode(led_pin,OUTPUT);
      digitalWrite(led_pin,LOW);
      if(new_ledstatus == led_on){
        digitalWrite(led_pin,HIGH);
      } 
        mainloopTimer = new_mainloopTimer;
        if (new_ledstatus == led_initial)
          ledstatus = led_off;
        ledstatus = new_ledstatus;
        led_old_status = led_initial;
      
  }
  

    void Leds::set_led_on(){
    digitalWrite(led_pin, HIGH);
        ledstatus = led_on;
    }
    void Leds::set_led_off(){
    digitalWrite(led_pin, LOW);
        ledstatus =led_off;
    }
    int Leds::get_led_status(){
      return (int) ledstatus;
    }
    int Leds::get_old_led_old_status(){
      return (int) led_old_status;
    }
    int Leds::get_led_pin(){
      return (int) led_pin;
    }
    void Leds::set_led_blink_on(int new_set_count, int new_period){
      if(ledstatus != led_blink_on){
        set_count = new_set_count * 2;
        period = new_period * 1000 / mainloopTimer;
        period /= 2; 
        counter = period;
        ledstatus =led_blink_on;
        if(digitalRead(led_pin)){
          set_count++;
        }
      }
    }
    void Leds::set_led_blink_off(int new_set_count, int new_period){
      if(ledstatus != led_blink_off){
        set_count = new_set_count * 2;        
        period = new_period * 1000 / mainloopTimer;
        period /= 2; 
        counter = period;
        ledstatus =led_blink_off; 
        if(digitalRead(led_pin)){
          set_count++;
        }
      }
    }
    
    void Leds::execute_led_blink(){
      if(ledstatus != led_old_status){
          if (ledstatus ==  led_off){
                digitalWrite(led_pin, LOW);
          }
          else if (ledstatus == led_on){
                digitalWrite(led_pin, HIGH);
          }
        led_old_status =ledstatus;
      }  
      else{ 
        if (ledstatus == led_blink_on ){                         
               if(counter <= 0){
                digitalWrite(led_pin, !digitalRead(led_pin)); 
                counter = period;
                set_count--;
              }
              else if (set_count < 0){
                  ledstatus = led_on;
              }
              else{
                counter--;
              }
            }
        else if (ledstatus ==  led_blink_off){
               if(counter <= 0){
                digitalWrite(led_pin, !digitalRead(led_pin)); 
                counter = period;
                set_count--;
              }
              else if (set_count <= 0){
                  ledstatus = led_off;
              }
              else{
                counter--;
              }
       }           
     }
  }
  
