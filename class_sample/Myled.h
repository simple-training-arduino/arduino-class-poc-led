
#ifndef Morse_h
#define Morse_h


#include "Arduino.h"

enum led_states{
  led_off,
  led_on,
  led_blink_on,
  led_blink_off,
  led_initial
};

class Leds {
 
  led_states ledstatus, led_old_status;
  int set_count;
  int counter;
  int mainloopTimer;
  int led_pin;
  int period;
 
  public:
    Leds(int new_led_pin, int new_mainloopTimer, int new_ledstatus );
    void set_led_on();
    void set_led_off();
    int get_led_status();
    int get_old_led_old_status();
    int get_led_pin();
    void set_led_blink_on(int new_set_count, int new_period);
    void set_led_blink_off(int new_set_count, int new_period);    
    void execute_led_blink();
};

#endif
